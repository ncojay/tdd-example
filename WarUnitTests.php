<?php

use PHPUnit\Framework\TestCase;

include('./War.php');
include_once('./WarDeck.php');
include_once('./Card.php');

class WarUnitTests extends TestCase {

    /**
     * @var War a war class
     */
    private $war;

    /**
     * Test if the deck has the correct amount of cards
     */
    public function testCreateDeck()
    {
        //Arrange
        $warDeck = new WarDeck();

        //Act
        $warDeck->create(4, 13);

        //Assert
        $this->assertEquals(count($warDeck->deck), 52, "The deck of war cards is not 52");
    }

    /**
     * The shuffled deck should be randomized
     */
    public function testDeckShuffled()
    {
        $warDeck = new WarDeck();
        $warDeck->create(4, 13);
        $comparisonDeck = $warDeck->deck;

        $warDeck->shuffle();

        $this->assertNotEquals($warDeck->deck, $comparisonDeck, "The deck did not shuffle");
    }

    /**
     * Test player creation
     */
    public function testPlayerCreationAndCardsDealt()
    {
        $war = new War();

        // Setup game with 2 players
        $war->setupGame();

        $this->assertEquals(count($war->players), 2, "There are not 2 players in the game");
        $this->assertEquals(count($war->players['player 1']), 26, "Player 1 does not have 26 cards");
        $this->assertEquals(count($war->players['player 2']), 26, "Player 2 does not have 26 cards");
    }

    /**
     * Test custom player number creation
     */
    public function testCustomPlayerCreationByNumber()
    {
        $war = new War();

        $war->setupGame(4);

        $this->assertEquals(count($war->players), 4, "There are not 4 players in the game");
        $this->assertEquals(count($war->players['player 1']), 13, "Player 1 does not have 13 cards");
        $this->assertEquals(count($war->players['player 2']), 13, "Player 2 does not have 13 cards");
        $this->assertEquals(count($war->players['player 3']), 13, "Player 3 does not have 13 cards");
        $this->assertEquals(count($war->players['player 4']), 13, "Player 4 does not have 13 cards");
    }

    /**
     * Test custom player number creation
     */
    public function testCustomPlayerCreationByName()
    {
        $war = new War();
        $players = ["Jimbo", "Bob", "Hickory", "Dickory", "Dock"];
        $playerNumber = count($players);
        $dealFinalCards = 52 % $playerNumber;

        $war->setupGame($players);

        $this->assertEquals(count($war->players), $playerNumber, "There are not {$playerNumber} players in the game");
        // Dyanamically get the number of players and deal their cards
        $dealtCards = intval(52/$playerNumber);
        for ($i = 0; $i < $playerNumber; $i++) {
            $playersCards = $dealtCards;
            if ($dealFinalCards) {
                $playersCards++;
                $dealFinalCards--;
            }
            $this->assertEquals(count($war->players[$players[$i]]), $playersCards, "{$players[$i]} does not have expected cards");
        }
    }

    /**
     * Test player using tieGameValue throws an exception
     */
    public function testTieGameValuesException()
    {
        $war = new War();
        $players = ["Jimbo", $war->getTiedGameValue()];

        $this->expectException("Exception");
        $this->expectExceptionMessage("Players may not be named {$war->getTiedGameValue()}");

        $war->setupGame($players);
    }

    /**
     * Test for round win logic
     */
    public function testRoundWin()
    {
        $this->war = new War();
        $this->setPlayerTwoRoundWinner();

        $winner = $this->war->playRound();

        $this->assertEquals($winner, 'player 2', "Player 2 did not win the round");
    }

    /**
     * Checks to see if round winner receives cards
     */
    public function testWinnerReceivesCards()
    {
        $this->war = new War();
        $this->setPlayerTwoRoundWinner();

        $winner = $this->war->playRound();

        $this->assertEquals(count($this->war->players[$winner]), 27, "Player 2 does not have 27 cards after winning a round");
    }

    /**
     * Test for round tie logic
     */
    public function testRoundTie()
    {
        $this->war = new War();
        $this->setTie();

        $result = $this->war->pullAndCheckCards();

        $this->assertEquals($result, $this->war->getTiedGameValue(), "The round result was not a tie");
    }

    /**
     * Test for tie breaker playoff
     */
    public function testRoundTieBreaker()
    {
        $this->war = new War();
        $this->setTie()->setPlayerOneTieBreakWinner();
        
        $result = $this->war->playRound();

        $this->assertEquals($result, "player 1", "Player 1 did not win the tie breaker as expected"); 
    }

    /**
     * If a tie breaker results in a tie, it should run a tie breaker again
     */
    public function testConsecutiveTieBreakers()
    {
        $this->war = new War();
        $this->setTie();
        $this->war->players['player 1'][4]->rank = 13;
        $this->war->players['player 2'][4]->rank = 13;

        // Set player 2 as the tie break winner
        $this->war->players['player 1'][8]->rank = 13;
        $this->war->players['player 2'][8]->rank = 1;

        $result = $this->war->playRound();

        $this->assertEquals($result, "player 1", "Player 1 did not win the tie breaker as expected"); 
    }

    /**
     * When a tie breaker is won, all cards should go to the winner
     */
    public function testTieBreakerWinnerReceivesCards()
    {
        $this->war = new War();
        $this->setTie()->setPlayerOneTieBreakWinner();
        
        $this->war->playRound();

        $winnerCardsNumber = count($this->war->players['player 1']);

        $this->assertEquals($winnerCardsNumber, 31, "Player 1 should win 5 cards");
    }

    /**
     * When a player has no more cards, they lose
     */
    public function testWhenCardsAreExhaustedPlayerLoses()
    {
        $this->setupCompleteMatch();

        $result = $this->war->startGame();

        $this->assertEquals($result['Game Winner'], "player 2", "Player 2 did not win the round.");
        $this->assertEquals($result['Number of Rounds'], "3", "Player 2 did not win in 3 rounds");

    }

    /**
     * Winner should have all cards at the end of the game
     */
    public function testWinnerShouldHaveAllCards()
    {
        $this->setupCompleteMatch();

        $this->war->startGame();

        $this->assertEquals(count($this->war->players['player 2']), 6, 'The game winner does not have all 6 cards');
    }

    /**
     * Function for setting up a round with player 2 as predetermined winner
     */
    private function setPlayerTwoRoundWinner()
    {
        $this->war->setupGame(2);
        // Set player 2 as holding the winning card
        $this->war->players['player 1'][0]->rank = 12;
        $this->war->players['player 2'][0]->rank = 13;
    }

    /**
     * Function for setting up a round with players tied
     */
    private function setTie()
    {
        $this->war->setupGame(2);
        // Set tie
        $this->war->players['player 1'][0]->rank = 13;
        $this->war->players['player 2'][0]->rank = 13;

        return $this;
    }

    /**
     * Function for setting player 1 as the tie breaker winner
     */
    private function setPlayerOneTieBreakWinner()
    {
        $this->war->players['player 1'][4]->rank = 13;
        $this->war->players['player 2'][4]->rank = 1;

    }

    /**
     * Sets up all cards used in a match with 3 cards.
     */
    private function setupCompleteMatch()
    {
        $this->war = new War();
        $this->war->setupGame(2);
        $this->war->players['player 1'] = [];
        $this->war->players['player 1'][] = new Card(1, 1);
        $this->war->players['player 1'][] = new Card(1, 2);
        $this->war->players['player 1'][] = new Card(1, 3);


        $this->war->players['player 2'] = [];
        $this->war->players['player 2'][] = new Card(13, 1);
        $this->war->players['player 2'][] = new Card(13, 2);
        $this->war->players['player 2'][] = new Card(13, 3);
    }
}

?>