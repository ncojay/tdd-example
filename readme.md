A simple card game of war.  This game has no UI and can only be interacted with testing via PHPUnit.

Requirements
* Php 7.2+ 


To run tests, execute `php ./phpunit-8.3.phar WarUnitTests.php` from this directory.