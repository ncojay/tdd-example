<?php

include('./DeckInterface.php');
include_once('./Card.php');

class WarDeck implements DeckInterface
{
    /**
     * @var array An array of cards
     */
    public $deck = [];

    /** 
    * @param int numberOfSuits The number of suits to create
    * @param int numberOfRanks The number of card ranks to create
    */
    public function create(int $numberOfSuits, int $numberOfRanks)
    {
        $this->deck = [];
        foreach(range(1, $numberOfSuits) as $suit) {
            foreach(range(1, $numberOfRanks) as $rank) {
                $this->addCardToDeck($suit, $rank);
            }
        }
    }

    /**
     * Shuffles the deck
     */
    public function shuffle()
    {
        shuffle($this->deck);
    }

    /**
     * Deals a single card from the front of the array
     * @return Card returns a single card instance
     */
    public function deal()
    {
        return array_shift($this->deck);
    }

    /**
     * Adds a new card to the deck
     * @var int $suit The suit value
     * @var int $rank The rank value
     */
    private function addCardToDeck(int $suit, int $rank)
    {
        $card = new Card($suit, $rank);
        $this->deck[] = $card;
    }

}

?>