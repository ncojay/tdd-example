<?php

include('./WarDeck.php');

class War
{
    /**
     * @var array An array of players containing their cards
     * Defaults to two players
     */
    public $players = ['player 1' => [], 'player 2' => []];

    /**
     * @var WarDeck The WarDeck class.
     */
    public $warDeck;

    /**
     * @var int The number of cards to burn in the event of war
     */
    private $burnCards = 3;

    /**
     * @var array Cards played or burned during the tie breaker are cached to give to the winner.
     */
    private $cachedCards = [];

    /**
     * @var int The number of suits to be used in the game.
     */
    private $suits = 4;

    /**
     * @var int The number of ranks to be used in the game.
     */
    private $ranks = 13;

    /**
     * @var array An array of player names
     */
    private $playerKeys;

    /**
     * @var string The string used to delineate a tie
     */
    private $tiedGameValue = -99;

    /**
     * Returns the tied game value
     */
    public function getTiedGameValue()
    {
        return $this->tiedGameValue;
    }

    /**
     * Sets up and plays the game
     */
    public function play()
    {
        $this->setupGame();
        return $this->startGame();
    }

    /**
     * Sets the game in motion and returns the winner
     * @return string The winner's key name
     */
    public function startGame() {
        $numberOfRounds = 0;
        while(count($this->players) > 1) {
            $winner = $this->playRound();
            $numberOfRounds++;
        }
        
        return ['Game Winner' => $winner, 'Number of Rounds' => $numberOfRounds];
    }

    /**
     * Creates the deck, players (if necessary), shuffles the cards, and deals.
     * @var int|array $players The number of players or array of names to include in the game.
     */
    public function setupGame($players = null)
    {
        $this->warDeck = new WarDeck();
        $this->warDeck->create($this->suits, $this->ranks);
        
        if (!is_null($players) && ($players > 1)) {
            if (is_int($players)) {
                $this->createPlayersFromNumber($players);
            } elseif (is_array($players)) {
                $this->createPlayersFromArray($players);
            }
        }

        // Grabbing the keys for all players and adding it as a property
        $this->playerKeys = array_keys($this->players);

        $this->warDeck->shuffle();
        $this->dealCards();
    }

    /**
     * Plays a single round of War
     */
    public function playRound()
    {
        $roundWinner = $this->pullAndCheckCards();

        if ($roundWinner === $this->tiedGameValue) {
            $roundWinner = $this->playWarTieBreaker();
        }

        $this->players[$roundWinner] = array_merge($this->players[$roundWinner], $this->cachedCards);
        $this->cachedCards = [];

        // Check to see if player has cards still
        foreach ($this->playerKeys as $playerName) {
            $this->playerCardCheck($playerName);
        }

        return $roundWinner;
    }

    /**
     * Play a war tie breaker
     */
    public function playWarTieBreaker()
    {
        $winningPlayer = $this->tiedGameValue;

        while($winningPlayer === $this->tiedGameValue) {
            // Pull burn cards first
            for ($i = 1; $i <= $this->burnCards; $i++) {
                $cards = $this->pullCards();
                $this->cacheCards($cards);
            }

            // Check for winner
            $winningPlayer = $this->pullAndCheckCards();
        }

        return $winningPlayer;
    }

    /**
     * Pull and check cards
     * @return string The winner of the round
     */
    public function pullAndCheckCards()
    {
        $roundCards = $this->pullCards();
        $this->cacheCards($roundCards);

        return $this->checkRoundWinner($roundCards);
    }

    /**
     * Check all players against each other to determine the winner
     * @var array $roundCards The top card pulled from each active player.
     * @return string $winner The winner of the round
     */
    private function checkRoundWinner(array $roundCards)
    {
        // Set first player to compare against
        $players = array_keys($roundCards);
        $winner = $players[0];
        $highCard = $roundCards[$winner]->rank;

        // Find the highest card value, set to winner's variables
        for ($i = 1; $i < count($roundCards); $i++) {
            if ($highCard < $roundCards[$players[$i]]->rank) {
                $winner = $players[$i];
                $highCard = $roundCards[$players[$i]]->rank;
            } elseif ($highCard == $roundCards[$players[$i]]->rank) {
                $winner = $this->tiedGameValue;
            }
        }

        return $winner;
    }

    /**
     * Pulls cards from players
     * @return array An array of Card instances from each player
     */
    private function pullCards()
    {
        $roundCards = [];
        foreach ($this->players as $playerName => &$cards) {
            if ($this->playerCardCheck($playerName)) {  //Ensure player still has cards to play
                $roundCards[$playerName] = array_shift($cards);
            }
        }
        
        return $roundCards;
    }

    /**
     * Puts the cards in play during the round into the kitty
     * @var array $roundCards An associated array of Card objects keyed by players
     */
    private function cacheCards(array $roundCards)
    {
        $this->cachedCards = array_merge($this->cachedCards, array_values($roundCards));
    }

   /**
     * Checking to see if player has cards remaining to stay in game.
     * @var string $playerName The playerName used to access their card stack in the player array.
     * @return boolean True if player still has cards
     */
    private function playerCardCheck(string $playerName) {
        if (count($this->players[$playerName]) < 1) {
            $this->removePlayer($playerName);
            return false;
        }
        return true;
    }

    /**
     * Removes a player from the game when out of cards
     */
    private function removePlayer(string $playerName) {
        unset($this->players[$playerName]);
        unset($this->playerKeys[array_flip($this->playerKeys)[$playerName]]);
    }

    /**
     * Deals cards to all listed players
     */
    private function dealCards() {
        $i = 0;
        $numberOfPlayers = count($this->players);
        while ($card = $this->warDeck->deal()) {
            $this->players[$this->playerKeys[$i % $numberOfPlayers]][] = $card;
            $i++;
        }
    }

    /**
     * Uses a simple player numbering system to create players
     * @var int|array $player The number of players or array of player names
     */
    private function createPlayersFromNumber(int $players) {
        $this->players = [];
        for ($i = 1; $i <= $players; $i++) {
            $this->players["player $i"] = [];
        }
    }

    /**
     * Uses a simple player numbering system to create players
     * @var int|array $player The number of players or array of player names
     */
    private function createPlayersFromArray(array $players) {
        $this->players = [];

        // Don't allow people use the tiedGameValue.
        if (in_array($this->tiedGameValue, $players)) {
            throw new Exception("Players may not be named {$this->tiedGameValue}");
        }

        foreach($players as $player) {
            $this->players[$player] = [];
        }
    }
}

?>