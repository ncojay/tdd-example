<?php

include_once('./Card.php');

interface DeckInterface
{
    /**
     * Creates the cards used in the game
     * @var int $numberOfSuits The number of suits in the deck.
     * @var int $numberOfRanks The number of card ranks in the deck.
     */
    public function create(int $numberOfSuits, int $numberOfRanks);

    /**
     * Shuffles the deck
     */
    public function shuffle();

    /**
     * Deals a card.
     */
    public function deal();
}

?>