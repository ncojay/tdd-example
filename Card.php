<?php

class Card 
{
    /**
     * @var int The card's rank value
     */
    public $rank;

    /**
     * @var int The card's suit value
     */
    public $suit;

    /**
     * Constructor for class
     */
    public function __construct(int $rank = null, int $suit = null)
    {
        $this->rank = $rank;
        $this->suit = $suit;
    }

}

?>